@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		<div class="jumbotron">
			<h1>Edit offer</h1>
		</div>
		<div class="col-md-4">
			<form action="{{ route('offers.update', $offer->id) }}" method="post">
				{{ method_field('patch') }}
				<div class="form-group">
					<label for="currency_code">Selected currency</label>
					<select class="form-control" name="currency_code" id="currency_code">
						@foreach($currencies as $currency)
								@if ( $offer['currency_code'] == $currency->code ) 
								selected 
								@endif >							
							<option value="{{ $currency->code}}">{{ $currency->name}}</option>
						@endforeach		
					</select>						
				</div>			
				<div class="form-group">
					<label for="payment_id">Payment</label>
					<select class="form-control" name="payment_id" id="payment_id">
						@foreach($payments as $payment)
							<option value="{{ $offer->id}}"
								@if ( $offer['payment_id'] == $currency->code ) 
								selected 
								@endif >							
								{{ $payment->name}}
							</option>
						@endforeach	
					</select>						
				</div>		
				<div class="form-group">
					<label for="max">Max</label>
					<input type="text" name="max" class="form-control" value="{{ $offer->max }}">				
				</div>	
				<div class="form-group">
					<label for="min">Min</label>
					<input type="text" name="min" class="form-control" value="{{ $offer->min }}">				
				</div>	
				<div class="form-group">
					<label for="margin">Margin</label>
					<input type="text" name="margin" class="form-control" value="{{ $offer->margin }}">				
				</div>	
				<div class="form-group">
					<label for="final_offer_price">Final offer price</label>
					<input type="text" name="final_offer_price" class="form-control" disabled="disabled">				
				</div>				
				<button class="btn btn-primary" type="submit">Edit</button>
				{{ csrf_field() }}
			</form>
		</div>
	</div> 


@endsection