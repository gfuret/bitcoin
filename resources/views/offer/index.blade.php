@extends('layouts.app')

@section('content')

	<div class="jumbotron">
		<h1>Offers</h1>
		<form class="form-inline" name="offers_filter"  action={{ route('offers') }} method="get">
					<div class="form-group mb-2 margin-right-10px">
						<label class="search_amount  margin-right-5px">Amount:</label>
						<input type="text" class="form-control" id="search_amount" name="search_amount" placeholder="0">
					</div>
					<div class="form-group mb-2 margin-right-10px">
						<label class="search_currency margin-right-10px">Currency:</label>
						<select class="form-control" id="search_currency" name="search_currency">
							<option value="0">All</option>
							@foreach($currencies as $currency)
								<option value="{{ $currency->code}}" 
									@if ( isset($input['search_currency']) && $input['search_currency'] == $currency->code ) 
										selected 
									@endif >
									{{ $currency->name}}
								</option>
							@endforeach		
						</select>
					</div>
					<div class="form-group mb-2 margin-right-10px">
						<label class="search_payment margin-right-5px">Payment:</label>
						<select class="form-control" id="search_payment" name="search_payment">
							<option value="0">All</option>
							@foreach($payments as $payment)
								<option value="{{ $payment->id}}" >
									{{ $payment->name}}
								</option>
							@endforeach	
						</select>
					</div>
					<button type="submit" class="btn btn-primary btn-xs btn-margin-right">Filter search</button>
			
		</form>
	</div>
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif	
	<div class="col-md-12">
		@foreach($offers as $offer)
			<div class="col-md-12">
				<form class="form-inline all-offers"   action={{ route('trades.create') }} method="post">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="offer-label"><b>{{ $offer->currency_code }}</b></label>
					</div>
					<div class="form-group">
						<label class="offer-label long-130px"><b>Min offer:</b>{{ $offer->min }}</label>
					</div>
					<div class="form-group">
						<label class="offer-label long-130px"><b>Max offer:</b>{{ $offer->max }}</label>
					</div>
					<div class="form-group">
						<label class="offer-label"><b>Payment mehtod:</b> {{ $offer->payment->name }} </label>
					</div>
					<div class="form-group">
						<label class="offer-label"><b>Price per bitcoin:</b> {{ number_format(($offer->pricePerBitCoin()), 2, '.', ',') }} </label>
					</div>
					@if(Auth::user()->role_id == 2  )
						<div class="form-group">
						<label for="exampleInputEmail2" class="margin-right-5px">Amoun to buy: </label>
						<input type="text" class="form-control margin-right-10px" id="amount" name="amount" placeholder="0">
						<input type="hidden" value="{{$offer->id}}" id="offer_id" name="offer_id" >
						</div>
						<button type="submit" class="btn btn-primary btn-xs btn-margin-right">Buy offer</button>
					@endif	
				</form>
			</div>
		@endforeach 	
	</div>
@endsection

