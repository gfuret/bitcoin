@extends('layouts.app')

@section('content')
	<table class="table table-hover">
	  <tbody>
	    </tr>
			<tr>
				<th scope="row">Balance in bitcoin</th>
				<td colspan="2">{{ number_format($account->balance, 2, '.', ',')}} </td>
			</tr>
	  </tbody>
	</table>
	<div class="col-md-12">
		<h2>Active offers</h2>
		@foreach($offers as $offer)
			<div class="col-md-12">
				<form class="form-inline your-offers" action={{ route('trades.create') }} method="post">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="offer-label"><b>Currency:</b> {{ $offer->currency_code }}</label>
					</div>
					<div class="form-group">
						<label class="offer-label long-200px"><b>Min offer:</b> {{  number_format($offer->min, 2, '.', ',') }}</label>
					</div>
					<div class="form-group">
						<label class="offer-label long-200px"><b>Max offer:</b> {{  number_format($offer->max, 2, '.', ',') }}</label>
					</div>
					<div class="form-group">
						<label class="offer-label"><b>Price per BTC: </b> {{ number_format(($offer->pricePerBitCoin()), 2, '.', ',') }} </label>
					</div>
					<div class="form-group">
						<label class="offer-label"><b>Payment method:  </b>{{ $offer->payment->name }}</label>
					</div>
					<input type="hidden" value="{{$offer->id}}" id="offer_id" name="offer_id" >
					<a href={{ route('offers.edit', $offer->id) }} class="btn btn-primary btn-xs btn-margin-right"> Edit Offer </a>
					@if(!$offer->active)
						<a href={{ route('offers.enable', $offer->id) }} class="btn btn-primary btn-xs btn-margin-right"> Enable Offer </a>
					@else
						<a href={{ route('offers.disable', $offer->id) }} class="btn btn-warning btn-xs btn-margin-right"> Disable Offer </a>
					@endif
					
					<a href={{ route('offers.delete', $offer->id) }} class="btn btn-danger btn-xs btn-margin-right"> Delete Offer </a>
				</form>
			</div>
		@endforeach 		
	</div>
{{-- 	<div class="col-md-12">
		<h2><a href="#">Inactive offers</a></h2>
	</div> --}}
	<div class="col-md-12">
		<h2>Trades pending</h2>
		@foreach($trades as $trade)
			<div class="col-md-12">
				<form class="form-inline"   action={{ route('trades.update', $trade->id) }} method="post">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="trade-label"><b>Currency:</b> {{ $trade->currency_code }}</label>
					</div>
					<label class="trade-label">Amount: {{  number_format($trade->amount_fiat, 2, '.', ',') }} </label>
					<a href={{ route('trades.accept', $trade->id) }} class="btn btn-primary btn-xs btn-margin-right"> Accept </a>
					<a href={{ route('trades.reject', $trade->id) }} class="btn btn-danger btn-xs btn-margin-right"> Decline </a>
				</form>
			</div>
		@endforeach 
	</div>
{{-- 	<div class="col-md-12">
		<h2><a href="#">History of accepted and rejected</a></h2>
	</div> --}}
@endsection