@extends('layouts.app')

@section('content')
	<table class="table table-hover">
	  <tbody>
	    <tr>
	      <th scope="row">Name</th>
	      <td>{{ $user->name }}</td>
	    </tr>
	    <tr>
	      <th scope="row">Mail</th>
	      <td>{{ $user->email }}</td>
	    </tr>
	    <tr>
	      <th scope="row">Account type</th>
	      @if( $user->role_id == 3 )
	      	<td colspan="2">Guest</td>
	      @elseif( $user->role_id == 2 )
	      <td colspan="2">Registered user</td>
	      @else
	      	<td colspan="2">Admin</td>
	      @endif
	    </tr>
		@if( $user->role_id == 2 )
			<tr>
				<th scope="row">Balance in bitcoin</th>
				<td colspan="2">{{ number_format($account->balance, 2, '.', ',')}} </td>
			</tr>
	    @endif
	  </tbody>
	</table>
@endsection