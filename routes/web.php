<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('home');
});*/

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/', 'OfferController@index')->name('offers')->middleware('auth');
//ofers view
Route::get('/offers', 'OfferController@index')->name('offers')->middleware('auth');
Route::get('/offers/create', 'OfferController@create')->name('offers.create')->middleware('auth');
Route::post('/offers/store', 'OfferController@store')->name('offers.store')->middleware('auth');
Route::get('/offers/{id}/edit', 'OfferController@edit')->name('offers.edit')->middleware('auth');
Route::patch('/offers/{id}/update', 'OfferController@update')->name('offers.update')->middleware('auth');
Route::get('/offers/{id}/disable', 'OfferController@disable')->name('offers.disable')->middleware('auth');
Route::get('/offers/{id}/enable', 'OfferController@enable')->name('offers.enable')->middleware('auth');
Route::get('/offers/{id}/delete', 'OfferController@delete')->name('offers.delete')->middleware('auth');
//trade view
Route::post('/trades/create', 'TradesController@create')->name('trades.create')->middleware('auth');
Route::post('/trades/store', 'TradesController@store')->name('trades.store')->middleware('auth');
Route::post('/trades/{id}/update', 'TradesController@update')->name('trades.update')->middleware('auth');
Route::get('/trades/', 'TradesController@index')->name('trades')->middleware('auth');
Route::get('/trades/{id}/accept', 'TradesController@accept')->name('trades.accept')->middleware('auth');
Route::get('/trades/{id}/reject', 'TradesController@reject')->name('trades.reject')->middleware('auth');
//Dashboard main
Route::get('/Dashboard/', 'DashboardController@index')->name('dashboard')->middleware('auth');
//User
Route::get('/user/{id}/show', 'UserController@show')->name('user.show')->middleware('auth');


