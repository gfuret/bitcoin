<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('trade_partner_user_id');
            $table->double('amount_fiat', 15, 8)->nullable()->default(null);
            $table->double('amount_btc', 15, 8)->nullable()->default(null);
            $table->integer('payment_id')->nullable()->default(null);
            $table->integer('offer_id')->nullable()->default(null);
            $table->string('currency_code');
            $table->tinyInteger('status')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trades');
    }
}
