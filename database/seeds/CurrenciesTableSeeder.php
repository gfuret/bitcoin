<?php

use App\Currency;
use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
    	$EUR = new Currency();
        $EUR->code = 'EUR';
        $EUR->name = 'Euro';
        $EUR->save();

    	$USD = new Currency();
        $USD->code = 'USD';
        $USD->name = 'Dollar';
        $USD->save();

    	$GPB = new Currency();
        $GPB->code = 'GPB';
        $GPB->name = 'Pound sterling';
        $GPB->save();

    	$NGN = new Currency();
        $NGN->code = 'NGN';
        $NGN->name = 'Nigerian naira';
        $NGN->save();

    }
}
