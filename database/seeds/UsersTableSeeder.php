<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$users = factory(User::class, 10)->create();
        User::create([
            'name' 		=> 'Gabriel Furet',
            'email' 	=> 'gabriel.furet@gmail.com',
            'password'	=> bcrypt('12345678'),
            'password'	=> bcrypt('12345678'),
            'role_id'	=> Role::OWNER_ID,
        ]);
        User::create([
            'name' 		=> 'admin',
            'email' 	=> 'admin@admin.com',
            'password'	=> bcrypt('12345678'),
            'password'	=> bcrypt('12345678'),
            'role_id'	=> Role::OWNER_ID,
        ]);
        User::create([
            'name' 		=> 'Toivo',
            'email' 	=> 'toivo@admin.com',
            'password'	=> bcrypt('12345678'),
            'password'	=> bcrypt('12345678'),
            'role_id'	=> Role::OWNER_ID,
        ]);
        User::create([
            'name' 		=> 'Leo',
            'email' 	=> 'leo@admin.com',
            'password'	=> bcrypt('12345678'),
            'password'	=> bcrypt('12345678'),
            'role_id'	=> Role::GUESS_ID,
        ]);
    }
}
