<?php

use Illuminate\Database\Seeder;
use App\Account;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Account::create([
            'user_id' 	=> 1,
            'balance' 	=> (float) 5,
        ]);
        Account::create([
            'user_id' 	=> 2,
            'balance' 	=> (float) 5,
        ]);
        Account::create([
            'user_id' 	=> 3,
            'balance' 	=> (float) 5,
        ]);    
    }
}
