<?php

use App\Payment;
use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$amazonGiftCard = new Payment();
        $amazonGiftCard->name = 'Amazon gift card';
        $amazonGiftCard->save();

    	$walmartGiftCard = new Payment();
        $walmartGiftCard->name = 'Walmart gift card';
        $walmartGiftCard->save();

    	$paypal = new Payment();
        $paypal->name = 'Paypal';
        $paypal->save();

    	$skrill = new Payment();
        $skrill->name = 'Skrill';
        $skrill->save();
    }
}
