<?php

use App\Offer;
use Illuminate\Database\Seeder;

class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gabriel = new Offer();
        $gabriel->currency_code = 'USD';
        $gabriel->payment_id 	= 1;
        $gabriel->min 			= 5000.00;
        $gabriel->max 			= 10000.00;
        $gabriel->margin 		= 5;
        $gabriel->final_offer_price	= 1.05 * 5000 * 1;
        $gabriel->user_id       = 1;
        $gabriel->save();

        $evelin = new Offer();
        $evelin->currency_code = 'EUR';
        $evelin->payment_id 	= 1;
        $evelin->min 			= 5000.00;
        $evelin->max 			= 10000.00;
        $evelin->margin 		= 5;
        $evelin->final_offer_price	= 1.05 * 5000 * 1;
        $evelin->user_id        = 2;
        $evelin->save();

        $toivo = new Offer();
        $toivo->currency_code = 'EUR';
        $toivo->payment_id 	= 1;
        $toivo->min 			= 5000.00;
        $toivo->max 			= 10000.00;
        $toivo->margin 		= 5;
        $toivo->final_offer_price	= 1.05 * 5000 * 1;
        $toivo->user_id        = 2;
        $toivo->save();

        $leo = new Offer();
        $leo->currency_code = 'USD';
        $leo->payment_id 	= 1;
        $leo->min 			= 5000.00;
        $leo->max 			= 10000.00;
        $leo->margin 		= 5;
        $leo->final_offer_price	= 1.05 * 5000 * 1;
        $leo->user_id       = 2;
        $leo->save();

    }
}

