<?php

use App\ExchangeRate;
use Illuminate\Database\Seeder;

class ExchangeRatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$EUR = new ExchangeRate();
        $EUR->currency_code = 'EUR';
        $EUR->exchange_rate_dollar = 0.88;
        $EUR->save();

    	$USD = new ExchangeRate();
        $USD->currency_code = 'USD';
        $USD->exchange_rate_dollar = 1;
        $USD->save();

    	$GPB = new ExchangeRate();
        $GPB->currency_code = 'GPB';
        $GPB->exchange_rate_dollar = 0.75;
        $GPB->save();

    	$NGN = new ExchangeRate();
        $NGN->currency_code = 'NGN';
        $NGN->exchange_rate_dollar = 360;
        $NGN->save();
    }
}
