<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Offer;

class ExchangeRate extends Model
{
    static public function changeCurrencyToBitCoin( string $currencyCode, float $amount ){

    	$exchangeRate = self::where('currency_code', $currencyCode )->first();
    	
    	if ($exchangeRate === null) {
   			return null;	
		}

		return ( $exchangeRate->exchange_rate_dollar * $amount ) / Offer::BITCOIN_CURRENT_MARKET_PRICE_IN_DOLLARS; 
    }
}
