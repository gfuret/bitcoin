<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN_ID = 1;
    const OWNER_ID = 2;
    const GUESS_ID = 3;
}
