<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use App\Offer;
use App\Payment;
use Auth;

class OfferController extends Controller
{
    public function index(Request $request){

        $payments   = Payment::all();
        $currencies = Currency::all();
        $input      = $request->all();

        $offers 	= Offer::searchOffers( $input );

        if ($offers->isEmpty()){
            return view('offer.index', ['offers'=>[], 'payments'=> $payments, 'currencies'=>$currencies], );
        } 

    	return view('offer.index', compact('offers','payments','currencies','input'));
    }

    public function create(){

    	$currencies = Currency::all();
    	$payments 	= Payment::all();

        return view('offer.create', compact('payments','currencies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payments   = Payment::all();
        $currencies = Currency::all();
        $offer      = Offer::find( $id );
        return view('offer.edit', compact('offer','currencies','payments'));
    }

    public function update( Request $request,$offerId)
    {
        $offer = Offer::find( $offerId );
        $input = $request->all();   

        $finalOfferPrice = Offer::calculateFinalOffer( (float) $request->margin, $request->currency_code );

        $offer->update([
                'currency_code'=> $input['currency_code'] , 'payment_id'=> (int)  $input['payment_id'],
                'min'=> (float) $input['min'] , 'max'=> (float)  $input['max'],
                'margin'=> (float) $input['margin'] , 'max'=> (float)  $input['max'],
                'final_offer_price' => $finalOfferPrice 
       ]);

        return redirect('Dashboard');
    }

    public function store(Request $request){
    	
        $validatedData = $request->validate([
            'max'       => 'required|numeric',
            'min'       => 'required|numeric',
            'margin'    => 'required|numeric',
            'payment_id'    => 'required|numeric',
            'currency_code' => 'required',
        ]);

    	$input = $request->all();	

    	$finalOfferPrice = Offer::calculateFinalOffer( (float) $request->margin, $request->currency_code );

    	if( is_null($finalOfferPrice) || $request->min > $request->max )
    	{
    		return redirect('/offers/create');//todo error message
    	}

        $offer = Offer::create([
        		'currency_code'=> $request->currency_code , 'payment_id'=> (int)  $request->payment_id,
        		'min'=> (float) $request->min , 'max'=> (float)  $request->max,
        		'margin'=> (float) $request->margin , 'max'=> (float)  $request->max,
        		'final_offer_price' => $finalOfferPrice, 'user_id' => Auth::user()->id
        ]);

    	return redirect('Dashboard');
    }

    public function disable( $offerId ){

        $offer = Offer::find( $offerId );
        //todo if not found, if user doesnt own
        $offer->update([
            'active' => false
        ]);

        return redirect('Dashboard');
    }

    public function enable( $offerId ){

        $offer = Offer::find( $offerId );

        $offer->update([
            'active' => true
        ]);

        return redirect('Dashboard');
    }

    public function delete( $offerId ){

        $offer = Offer::findOrFail($offerId);

        $offer->delete();

        return redirect('Dashboard');

    }
    
}
