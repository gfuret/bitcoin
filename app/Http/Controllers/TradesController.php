<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;
use App\Trade;
use App\Account;
use App\ExchangeRate;
use Auth;
use Validator;

class TradesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Request $request )
    {
        $validatedData = $request->validate([
            'amount'    => 'required|numeric',
            'offer_id'  => 'required|numeric',
        ]);

        $input = $request->all();   

        $offer = Offer::find($input['offer_id']);

        if($input['amount'] < $offer->min || $input['amount'] > $offer->max ){
            return redirect('/offers');
        }
        
        $trade = Trade::create([
                'trade_partner_user_id' => Auth::user()->id,
                'amount_fiat'           => $input['amount'] ,
                'amount_btc'            => ExchangeRate::changeCurrencyToBitCoin( $offer->currency_code, (float) $input['amount'] ) ,
                'payment_id'            => $offer['payment_id'] ,
                'status'                => Trade::STATUS_PENDING,
                'currency_code'         => $offer->currency_code,
                'offer_id'              => $input['offer_id'],
        ]);

        return redirect('/offers');
    }

    /**
     * Accept trade, increase account balance
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function accept($tradeId)
    {
        $trade = Trade::find( $tradeId ); 

        $seller = Account::where('user_id',Auth::user()->id)->first();

        $buyer = Account::where('user_id', $trade->trade_partner_user_id )->first();        

        $trade->update([
                'status'=> Trade::STATUS_COMPLETE
        ]);   

        $seller->update([ 'balance' => $trade->amount_btc + $seller->balance ]);

        $buyer->update([ 'balance' => $trade->amount_btc + $buyer->balance ]);

        return redirect('Dashboard'); 

    }

    /**
     * Reject trade, increase account balance
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject($tradeId)
    {
        $trade = Trade::find( $tradeId ); 
        $trade->update([
                'status'=> Trade::STATUS_CANCEL
        ]);   
        return redirect('Dashboard'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
