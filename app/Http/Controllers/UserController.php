<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Account;
use App\Role;

class UserController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($userId)
    {
    	$user 		= User::find($userId);
    	$account 	= Account::where('user_id',$userId)->first();
        return view('user.show', compact('user','account'));
    }

}
