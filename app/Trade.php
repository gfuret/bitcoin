<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Offer;

class Trade extends Model
{
    const STATUS_PENDING 	= 1;
    const STATUS_CANCEL 	= 2;
    const STATUS_COMPLETE 	= 3;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'currency_code', 'payment_id', 'trade_partner_user_id', 'amount_fiat', 'amount_btc', 'status', 'offer_id',
	];

    public function offer()
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }

	static function userTrades( int $userId ){

		$offers = Offer::where('user_id', $userId )->pluck('id')->toArray();

		if(empty($offers))
		{
			return [];
		}
		return self::whereIn('offer_id', $offers )
					->where('status', self::STATUS_PENDING)
					->get();
	}
}
