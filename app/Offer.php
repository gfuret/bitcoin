<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ExchangeRate;
use App\Trade;
use App\Payment;
use Auth;

class Offer extends Model
{
	const BITCOIN_CURRENT_MARKET_PRICE_IN_DOLLARS = 3000;
    const ACTIVE_OFFER      = 1;
    const INACTIVE_OFFER    = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'currency_code', 'payment_id', 'min', 'max', 'margin', 'final_offer_price', 'user_id', 'amount', 'active',
    ];

    static public function calculateFinalOffer( float $margin, string $currencyCode )// :float
    {
    	$exchangeRate = ExchangeRate::where('currency_code', $currencyCode )->first();

    	if ($exchangeRate === null) {
   			return null;	
		}

    	$marginPercentage = ($margin * 0.01) + 1;

    	return (float) self::BITCOIN_CURRENT_MARKET_PRICE_IN_DOLLARS * $marginPercentage * $exchangeRate->exchange_rate_dollar;
    }

    static public function searchOffers( array $filters ){

        $query  = self::query();
        $query->where('user_id', '!=', Auth::user()->id );
        $query->where('active', self::ACTIVE_OFFER );
        if( ! empty($filters['search_currency']) ){
            $query->where('currency_code' , $filters['search_currency'] );
        }

        if( ! empty($filters['search_payment']) ){
            $query->where('payment_id' , $filters['search_payment'] );
        }
        if( ! empty($filters['search_amount']) ){
            $query->where('max' , '>=', $filters['search_amount'] );
            $query->where('min' , '<=', $filters['search_amount'] );
        }       
        return $query->get();
    }

    public function pricePerBitCoin(){
        return $this->final_offer_price / self::BITCOIN_CURRENT_MARKET_PRICE_IN_DOLLARS;
    }

    static public function userOffers( int $userId ){

        $query  = self::query();
        $query->where('user_id', $userId );     
        return $query->get();
    }


    public function payment(){
        return $this->belongsTo(Payment::class,'payment_id');
    }

}
